# KubeFlow Automator for Kubernetes Cluster Setup

This project demonstrates an automated workflow for setting up a Kubernetes cluster using Terraform, Ansible, Docker, and AWS. It's designed to simplify the process of deploying and managing Kubernetes clusters, making it ideal for developers and system administrators.

## Prerequisites
### AWS
- **AWS Account**: Create a free AWS account [here](https://aws.amazon.com/free/)
- **IAM Role Creation**: Follow this [guide](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create.html) to create an IAM role with admin access
- **AWS CLI Configuration**: Configure your AWS CLI with the following steps. More details [here](https://docs.aws.amazon.com/cli/latest/reference/configure/)
- **Key Pair**: Generate a key pair for SSH access as described [here](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/create-key-pairs.html) and named the public key `k8s-cluster-public-key` for integration with Terraform configurations
``` 
$ aws configure
AWS Access Key ID [None]: accesskey
AWS Secret Access Key [None]: secretkey
Default region name [None]: us-west-2 #define another region as needed
Default output format [None]:
```
- **State Storage**: Create a S3 bucket if using AWS as the terraform state backend. Details [here](https://docs.aws.amazon.com/AmazonS3/latest/userguide/creating-bucket.html)

### Terraform
Install terraform in your local or remote server:
- masOS
```
brew tap hashicorp/tap
brew install hashicorp/tap/terraform
```
- Other Operating Systems: [Terraform Installation Guide](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)

### Ansible
Ansilbe requires Python, ensure Python3 is installed on both the local/remote server(where Ansible will run) and the target servers. Download Python [here](https://www.python.org/downloads/).

### SSH
SSH access is required for secure communication with EC2 instances.

## Master Node and Worker Node
In each node of the Kubernetes cluster, there are three key processes must be installed:
- container runtime, responsible for running containers
- kubelet, an agent running on each node in the cluster, responsible for managing health of containers and pods
- kube-proxy, a network proxy that runs on each node in the cluster, allows network communication to and from pods according to the service rules

### Master Node(Control Plane)
The Master node is responsible for managing the Kubernetes cluster. It includes components like:
- **API Server** - Serves as the front end for Kubernetes, handling internal cluster communication
- **Scheduler** - Responsible for scheduling tasks to worker nodes, hanlding requests from the API Server, e.g. start a new pod and decide where to put the new pod between nodes
- **Controller Manager** - Oversees various controllers that regulate cluster state, reacting to changes within the cluster, e.g. detect cluster state changes and make requests to Scheduler
- **etcd** - Key-value store for Kubernetes cluster data, ensuring data consistency

### Worker Node
Worker nodes are the server where pods(workloads) are deployed. Thay are managed by the master nodes and include the necessary components to run pods assigned by the master node.

### Container Runtime
- **containerd** is used for its lightweight feature. Other options, refer to [container runtime documentation](https://kubernetes.io/docs/setup/production-environment/container-runtimes/)
### CNI, Container Netwokring Interface
- **Weave Net** is implemented for pod-to-pod communication within the cluster. Other options are available [here](https://kubernetes.io/docs/concepts/cluster-administration/addons/#networking-and-network-policy)

### Setup Scripts
`entry-master-script.sh` and `entry-worker-script.sh` detail the installation process. Swap is off by default in Ubuntu. For other OS, apply `swapoff -a` as needed.

## Cluster Setup
1. Initialize Terraform
- Navigate to the Terraform directory and initialize:
```
cd terraform
terraform init

```

2. Set up Variables
- Store configuration variables in a `terraform.tfvars` file in the terraform directory:
```
touch terraform.tfvars
``` 
In the file:
```
admin_ssh_list = ["0.0.0.0/0"] #customize as needed
k8s-cluster-private-key = "<key_location>"
ansible_destination = "<file_copied_to_ansible_server>"
file_location = "<file_loction_in_ansible_server>"
```
Overwrite other default values in this file as needed.

3. Plan and Apply Configuration
- Preview changes with 
```
terraform plan
```
- Apply Configuration
```
terraform apply
```
- To auto-approve changes, use 
```
terraform apply --auto-approve
```

4. Post-Setup Verification
- ssh into the master node:
```
ssh -i <public_key_location> ubuntu@<instance_public_IP_address>
```
- Verify if the Kubernetes cluster is set up correctly by running:
`kubectl get nodes`
This should list all the nodes in the Kubernetes cluster.

5. Testing on the Cluster
- Deploy application for testing, example:
```
kubectl apply -f nginx-deployment.yaml
kubectl apply -f nginx-service-ingress.yaml
```

### Logs & Debugs
Logs can be found at `/var/log/entry-master-script.log` on the master node and `/var/log/entry-worker-script.log` on the worker nodes. These logs are crucial for troubleshooting issues that may arise within the cluster. 
Debugging whith kubectl:
- To inspect the logs of a specific pod, use:
```
kubectl logs <pod_name>
```
- For a detailed description and status of a Kubernetes resource (like pods, services, deployments), use:
kubectl describe <type of resource> <resource name>, example:
```
kubectl describe pod <pod_name>
```

### Monitoring
Monitor the status and health of your EC2 instances by running the provided Python script:
```
python3 node-status-checks.py
```
This script offers insights into the operational status of each node, helping in proactive maintenance and performance monitoring.

### Alternetive: Using Docker & Jenkins
You can set up the KubeFlow Automator environment using the provided Dockerfile
- Build the Docker Image
`docker build -t kubeflow:1.0 .`
Customize the tag as needed or push it to Docker Hub for public access:
`docker build -t <docker_username>/kubeflow:1.0 .`
By default, Ubuntu has python3 pre-installed in the OS, if other OS is used, adjust the Dockerfile accordlingly.
- Proceed with the Cluster Setup guidelines

### Jenkins Configuration
Install and run Jenkins using Docker in a detach mode:
```
docker run -p 8080:8080 -p 50000:50000 -d jenkins/jenkins:lts 
```
Enhanced Docker Integration: For Jenkins to execute Docker commands and access files from the host machine:
```
docker run -p 8080:8080 -p 50000:50000 -d -v jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -v $(which docker):/usr/bin/docker jenkins/jenkins:lts 
```
This command mounts the Docker socket and the Docker binary from the host to the container, allowing Jenkins to use Docker on the host machine. More details [here](https://hub.docker.com/r/jenkins/jenkins).

Additional Resources
- [Terraform AWS Provider Documentation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs)
- [Ansible User guide](https://docs.ansible.com/ansible/latest/index.html)
- [Kubernetes Documentation](https://kubernetes.io/docs/home/)
- [Docker Documentation](https://docs.docker.com/get-started/overview/)
- [OpenSSH Manual Pages](https://www.openssh.com/manual.html)
- [Jenkins Documentation](https://www.jenkins.io/doc/)

