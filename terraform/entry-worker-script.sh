#!/bin/bash
exec > /var/log/entry-worker-script.log 2>&1
set -x

echo "Loading kernel modules..."
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF
sudo modprobe overlay
sudo modprobe br_netfilter

echo "Setting sysctl parameters..."
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
sudo sysctl --system

echo "Installing & Configuring containerd..."
sudo apt-get update
sudo apt-get -y install containerd
if [ $? -eq 0 ]; then
  echo "Downloaded containerd"
  sudo mkdir -p /etc/containerd
  containerd config default | sudo tee /etc/containerd/config.toml
else
  echo "Error downloading containerd"
  exit 1
fi

echo "Configuring systemd cgroup driver..."
sudo sed -i 's/SystemdCgroup = false/SystemdCgroup = true/g' /etc/containerd/config.toml
if [ $? -eq 0 ]; then
  echo "systemd cgroup driver is configured"
  sudo systemctl restart containerd
else
  echo "Error configuring systemd cgroup driver"
  exit 1
fi

echo "Installing Kubernetes components..."
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gpg
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
if [ $? -eq 0 ]; then
  echo "Installed kubelet kubeadm kubectl"
  sudo apt-mark hold kubelet kubeadm kubectl
else
  echo "Error installing kubelet kubeadm kubectl"
  exit 1
fi
