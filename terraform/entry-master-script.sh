#!/bin/bash
exec > /var/log/entry-master-script.log 2>&1
set -x

echo "Installing dependenies..."
wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install terraform

echo "Loading kernel modules..."
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF
sudo modprobe overlay
sudo modprobe br_netfilter

echo "Setting sysctl parameters..."
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
sudo sysctl --system

echo "Installing & Configuring containerd..."
sudo apt-get update
sudo apt-get -y install containerd
if [ $? -eq 0 ]; then
  echo "Downloaded containerd"
  sudo mkdir -p /etc/containerd
  containerd config default | sudo tee /etc/containerd/config.toml
else
  echo "Erroring installing containerd"
  exit 1
fi

echo "Configuring systemd cgroup driver..."
sudo sed -i 's/SystemdCgroup = false/SystemdCgroup = true/g' /etc/containerd/config.toml
if [ $? -eq 0 ]; then
  echo "systemd cgroup driver is configured"
  sudo systemctl restart containerd
else
  echo "Error configuring systemd cgroup driver"
  exit 1
fi

echo "Installing Kubernetes components..."
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gpg
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
# edit the version here
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
if [ $? -eq 0 ]; then
  echo "Installed kubelet kubeadm kubectl"
  sudo apt-mark hold kubelet kubeadm kubectl
else
  echo "Error installing kubelet kubeadm kubectl"
  exit 1
fi

echo "Initializing Kubernetes cluster with kubeadm..."
sudo kubeadm init
if [ $? -eq 0 ]; then
  echo "Initialized Kubernetes cluster"
  echo "Configuring kubectl..."
  mkdir -p /home/ubuntu/.kube
  sudo cp -i /etc/kubernetes/admin.conf /home/ubuntu/.kube/config
  sudo chown ubuntu:ubuntu /home/ubuntu/.kube/config
  if [ $? -eq 0 ]; then
    echo "Configured .kube/config"
    export KUBECONFIG=/home/ubuntu/.kube/config
    echo "export KUBECONFIG=/home/ubuntu/.kube/config" >> /home/ubuntu/.bashrc
  else
    echo "Error configuring kubelet"
    exit 1
  fi
else
  echo "Failed to initialize Kubernetes cluster"
  exit 1
fi

echo "Setting up network for Kubernetes cluster..."
wget https://github.com/weaveworks/weave/releases/download/v2.8.1/weave-daemonset-k8s.yaml -O weave.yaml 
if [ $? -eq 0 ]; then
  echo "Downloaded weave net manifest"
  kubectl apply -f /weave.yaml
  if [ $? -eq 0 ]; then
    echo "weave net applied successfully"
  else
    echo "Error applying weave net"
    exit 1
  fi
else
  echo "Error downloading weave net manifest"
  exit 1
fi

sudo kubeadm token create --print-join-command > /home/ubuntu/joinnode.sh
if [ $? -ne 0 ]; then
  echo "Failed to create kubeadm token"
  exit 1
fi
echo "Created kubeadm token"

sudo chmod 744 /home/ubuntu/joinnode.sh