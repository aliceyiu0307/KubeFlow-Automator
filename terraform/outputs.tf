output "master-node-public-ip" {
    value = aws_instance.master-node-server.public_ip
}

output "server-public-ip" {
    value = [
        aws_instance.worker-node-server-1.public_ip,
        aws_instance.worker-node-server-2.public_ip
    ]
}
