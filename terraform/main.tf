provider "aws" {
    region = var.region
}

variable "region" {
  default = "us-west-2"
}
variable "avail_zone" {
    default = "us-west-2b"
}
variable "vpc_cidr_block" {
    default = "172.31.0.0/16"
}
variable "subnet_cidr_block" {
    default = "172.31.1.0/24"
}
variable "env_prefix" {
    default = "dev"
}
variable "admin_ssh_list" {
    type = list(string)
}
variable "local_path" {
    type = string
}
variable "master_instance_type" {
    type = string
    default = "t2.medium"
}
variable "worker_instance_type" {
    type = string
    default = "t2.large"
}
variable "k8s-cluster-private-key" {
    type = string
}
variable "ansible_destination" {
    type = string
}
variable "file_location" {
    type = string
}


resource "aws_vpc" "k8s-cluster-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name: "${var.env_prefix}-k8s-cluster-vpc"
  }
}

resource "aws_subnet" "k8s-cluster-subnet-1" {
    vpc_id = aws_vpc.k8s-cluster-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    tags = {
        Name = "${var.env_prefix}-k8s-cluster-subnet-1"
    }
}

resource "aws_internet_gateway" "k8s-cluster-igw" {
    vpc_id = aws_vpc.k8s-cluster-vpc.id 
    tags = {
        Name: "${var.env_prefix}-k8s-cluster-igw"
    }
}

resource "aws_default_route_table" "k8s-main-rtb" {
    default_route_table_id = aws_vpc.k8s-cluster-vpc.default_route_table_id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.k8s-cluster-igw.id
    }
    tags = {
        Name: "${var.env_prefix}-k8s-main-rtb"
    }
}

resource "aws_security_group" "master-node-sg" {
    name = "master-node-sg"
    description = "traffic rules configuration for master node"
    vpc_id = aws_vpc.k8s-cluster-vpc.id

    ingress {
        description = "ssh connection"
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = var.admin_ssh_list
    }

    ingress {
        description = "kube scheduler"
        from_port = 10259
        to_port = 10259
        protocol = "tcp"
        cidr_blocks = [var.vpc_cidr_block]
    }

    ingress {
        description = "kube controller manager"
        from_port = 10257
        to_port = 10257
        protocol = "tcp"
        cidr_blocks = [var.vpc_cidr_block]
    }

    ingress {
        description = "k8s API server"
        from_port = 6443
        to_port = 6443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        description = "kubelet"
        from_port = 10250
        to_port = 10250
        protocol = "tcp"
        cidr_blocks = [var.vpc_cidr_block]
    }

    ingress {
        description = "weave-net"
        from_port = 6783
        to_port = 6783
        protocol = "tcp"
        cidr_blocks = [var.vpc_cidr_block]
    }

    ingress {
        description = "etcd"
        from_port = 2379
        to_port = 2380
        protocol = "tcp"
        cidr_blocks = [var.vpc_cidr_block]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    }

    tags = {
      Name = "master-node-sg"
    }
  
}

resource "aws_security_group" "worker-node-sg" {
    name = "worker-node-sg"
    description = "traffic rules configuration for worker node"
    vpc_id = aws_vpc.k8s-cluster-vpc.id

    ingress {
        description = "ssh connection"
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = var.admin_ssh_list
    }

    ingress {
        description = "NodePort"
        from_port = 30000
        to_port = 32767
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        description = "kubelet"
        from_port = 10250
        to_port = 10250
        protocol = "tcp"
        cidr_blocks = [var.vpc_cidr_block]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    }

    tags = {
        Name = "worker-node-sg"
    }
  
}

resource "aws_instance" "master-node-server" {
    ami = "ami-0efcece6bed30fd98"
    instance_type = var.master_instance_type
    subnet_id = aws_subnet.k8s-cluster-subnet-1.id
    vpc_security_group_ids = [aws_security_group.master-node-sg.id]
    availability_zone = var.avail_zone

    associate_public_ip_address = true
    key_name = "k8s-cluster-public-key"

    user_data = file("entry-master-script.sh")

    tags = {
        Name = "${var.env_prefix}-master-node-server"
    }

    depends_on = [
        aws_instance.worker-node-server-1,
        aws_instance.worker-node-server-2
    ]
}

resource "terraform_data" "master-node-confugration" {
    triggers_replace = [
      aws_instance.master-node-server.public_ip
    ]
    provisioner "local-exec" {
        working_dir = "../ansible"
        command = "sleep 60 && ansible-playbook --inventory ${aws_instance.master-node-server.public_ip}, --private-key ${var.k8s-cluster-private-key} --user ubuntu master-node-configuration.yaml -e ansible_destination='${var.ansible_destination}'"
    }
}

resource "aws_instance" "worker-node-server-1" {
    ami = "ami-0efcece6bed30fd98"
    instance_type = var.worker_instance_type
    subnet_id = aws_subnet.k8s-cluster-subnet-1.id
    vpc_security_group_ids = [aws_security_group.worker-node-sg.id]
    availability_zone = var.avail_zone

    associate_public_ip_address = true
    key_name = "k8s-cluster-public-key"

    user_data = file("entry-worker-script.sh")

    tags = {
        Name = "${var.env_prefix}-worker-node-server-1"
    }
}

resource "aws_instance" "worker-node-server-2" {
    ami = "ami-0efcece6bed30fd98"
    instance_type = var.worker_instance_type
    subnet_id = aws_subnet.k8s-cluster-subnet-1.id
    vpc_security_group_ids = [aws_security_group.worker-node-sg.id]
    availability_zone = var.avail_zone

    associate_public_ip_address = true
    key_name = "k8s-cluster-public-key"

    user_data = file("entry-worker-script.sh")

    tags = {
        Name = "${var.env_prefix}-worker-node-server-2"
    }
}

resource "terraform_data" "worker-node-confugration" {
    triggers_replace = [
      terraform_data.master-node-confugration
    ]
    provisioner "local-exec" {
        working_dir = "../ansible"
        command = "ansible-playbook --inventory '${aws_instance.worker-node-server-1.public_ip},${aws_instance.worker-node-server-2.public_ip},' --private-key ${var.k8s-cluster-private-key} --user ubuntu worker-node-configuration.yaml -e file_location='${var.file_location}'"
    }
}

# comment this out if not using cloud as the state store
terraform {
  backend "s3" {
    bucket = "terraform-state.storage"
    key = "dev/state/terraform.tfstate"
    region = "us-west-2"
    encrypt = true
  }
}
